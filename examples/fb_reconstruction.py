import matplotlib
matplotlib.use('Qt5Agg')
from tqdm import tqdm
from crbtk import projection
from crbtk.rebin import helicalToSingleSlice
import astra
import numpy as np
import SimpleITK as sitk
import matplotlib.pyplot as plt
import pandas as pd
#
def ReconstructFanBeam(filename, dictnpy, metadata, outfilename):
    print filename
    sino = sitk.GetArrayFromImage(sitk.ReadImage(filename))
    d = np.load(dictnpy)

    srcgeom=d.item().get('srcgeom')
    dectgeom=d.item().get('dectgeom')
    fov = dectgeom['FOV'] * 1
    # hufact = d.item().get('calfactor')
    hufact=1
    det_width = dectgeom['ColumnSpacing']
    det_count = dectgeom['Columns']
    source_origin = srcgeom['sod']
    origin_det =  srcgeom['sdd'] - srcgeom['sod']
    # origin_det=0
    angles =np.array(metadata['DetectorFocalCenterAngularPosition'][metadata['SliceNumber']==0])
    # angles=np.linspace(0, 2*np.pi, 2304) - 3.907
    # plt.plot(angles1)
    # plt.plot(angles)
    # plt.show()
    # plt.pause(5)

    image = []
    for i in tqdm(range(sino.shape[0])):
        pg = astra.create_proj_geom(
            'fanflat',
            det_width,
            det_count,
            angles,
            source_origin,
            origin_det
        )
        vg = astra.create_vol_geom(512, 512, -fov/2., fov/2., -fov/2., fov/2.)


        rec_id = astra.data2d.create('-vol', vg)
        sino_id = astra.data2d.create('-sino', pg)
        projtor_id = astra.create_projector('line_fanflat', pg, vg)

        astra.data2d.store(sino_id, sino[i])

        ## SIRT ##
        # cfg = astra.astra_dict('SIRT_CUDA')
        # cfg['ProjectionDataId'] = sino_id
        # cfg['ReconstructionDataId'] = rec_id

        ## FBP ##
        cfg = astra.astra_dict('FBP')
        cfg['ProjectionDataId'] = sino_id
        cfg['ReconstructionDataId'] = rec_id
        cfg['ProjectorId'] = projtor_id
        # cfg['FilterType'] = 'parzen'

        algo_id = astra.algorithm.create(cfg)
        astra.algorithm.run(algo_id, 500)

        im = astra.data2d.get(rec_id)
        im = (im - hufact) * 1000 / hufact
        # im = np.flipud(im)
        image.append(im[None,:,:])

        astra.data2d.delete(sino_id)
        astra.data2d.delete(rec_id)
        astra.algorithm.delete(algo_id)

    del sino
    image = np.concatenate(image, axis=0)
    image = sitk.GetImageFromArray(image)
    sitk.WriteImage(image, outfilename)


    pass

# #
def ReconstructParBeam(filename, dictnpy, metadata, outfilename):
    print filename
    sino = sitk.GetArrayFromImage(sitk.ReadImage(filename))
    d = np.load(dictnpy)

    srcgeom=d.item().get('srcgeom')
    dectgeom=d.item().get('dectgeom')
    fov = dectgeom['FOV'] *1.2
    # hufact = d.item().get('calfactor')
    hufact=1
    det_width = dectgeom['ColumnSpacing']
    det_count = dectgeom['Columns']
    source_origin = srcgeom['sod']
    origin_det =  srcgeom['sdd'] - srcgeom['sod']
    # origin_det=0
    angles =np.array(metadata['DetectorFocalCenterAngularPosition'][metadata['SliceNumber']==0])
    # angles=np.linspace(0, 2*np.pi, 2304)

    image = []
    for i in tqdm(range(sino.shape[0])):
        pg = astra.create_proj_geom(
            'parallel',
            det_width,
            det_count,
            angles
            # source_origin,
            # origin_det
        )
        vg = astra.create_vol_geom(512, 512, -fov/2., fov/2., -fov/2., fov/2.)


        rec_id = astra.data2d.create('-vol', vg)
        sino_id = astra.data2d.create('-sino', pg)
        projtor_id = astra.create_projector('line', pg, vg)

        astra.data2d.store(sino_id, sino[i])

        # SIRT ##
        cfg = astra.astra_dict('SIRT_CUDA')
        cfg['ProjectionDataId'] = sino_id
        cfg['ReconstructionDataId'] = rec_id

        # ## FBP ##
        # cfg = astra.astra_dict('FBP')
        # cfg['ProjectionDataId'] = sino_id
        # cfg['ReconstructionDataId'] = rec_id
        # cfg['ProjectorId'] = projtor_id
        # # cfg['FilterType'] = 'parzen'

        algo_id = astra.algorithm.create(cfg)
        astra.algorithm.run(algo_id, 500)

        im = astra.data2d.get(rec_id)
        im = (im - hufact) * 1000 / hufact
        # im = np.flipud(im)
        image.append(im[None,:,:])

        astra.data2d.delete(sino_id)
        astra.data2d.delete(rec_id)
        astra.algorithm.delete(algo_id)

    del sino
    image = np.concatenate(image, axis=0)
    image = sitk.GetImageFromArray(image)
    sitk.WriteImage(image, outfilename)

    pass

if __name__ == '__main__':
    # rootdir = "/media/storage/Source/Repos/CT-Rebinning-Toolkit/ProjectionData/10.Batch_01/testing/"
    # subdirs = ['full_DICOM-CT-PD', 'quarter_DICOM-CT-PD', 'quarter_DICOM-CT-PD_oneres', 'quarter_DICOM-CT-PD_Processed']
    rootdir = "/media/storage/Source/Repos/CT-Rebinning-Toolkit/ProjectionData/"
    d='L109_rebinned_par.nii.gz'
    # for d in subdirs:
    #     ReconstructFanBeam(d + ".nii.gz", d + ".dict.npy", d + "_out.nii.gz")

    # ReconstructFanBeam(rootdir + d, rootdir+ d.replace('.nii.gz', '.npy'), pd.read_csv(rootdir+d.replace('.nii.gz', '.csv')), rootdir+d.replace('.nii.gz', '_out.nii.gz'))
    ReconstructParBeam(rootdir + d, rootdir+ d.replace('.nii.gz', '.npy'), pd.read_csv(rootdir+d.replace('.nii.gz', '.csv')), rootdir+d.replace('.nii.gz', '_out.nii.gz'))