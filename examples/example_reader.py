"""
This example reads projection and display them in visdom
"""

from crbtk.projection import Reader
import numpy as np
import visdom
import numpy as np
from torch.tensor import _TensorBase

vis = visdom.Visdom(port=80)

def Normalize(array, lowerbound=None, upperbound=None, range=[0, 1]):
    """
    Description
    -----------
      Clip the image with lowerbound and upperbound and then normalize it to the given range.

    :param np.ndarray array:
    :param float lowerbound:
    :param float upperbound:
    :param list range:
    :return: np.ndarray
    """

    assert isinstance(array, np.ndarray)
    assert isinstance(range, list)

    t = np.array(array, dtype=float)
    if not (lowerbound is None or upperbound is None):
        assert lowerbound < upperbound, "Lower bound must be less than upper bound!"
        t = np.clip(t, lowerbound, upperbound)
        t = t - lowerbound
    else:
        t = array
        t = t - t.min()

    if not (upperbound is None):
        t = t * (range[1] - range[0]) / (upperbound -lowerbound) - range[0]
    else:
        t = t * (range[1] - range[0]) / t.max() - range[0]

    return t


def Visualize2D(*args, **kwargs):
    """Visualize2D(*args, **kwargs) -> None

    Keywords:
    :keyword int axis   Which axis to display
    :keyword str env    Visdom environment
    :keyword str prefix Window prefix of visdom display windows
    :keyword int nrow   Number of images per row
    :keyword iter displayrange  Display values range
    :keyword iter indexrange    Index range of the displayed image

    :return: None
    """
    axis = kwargs['axis'] if kwargs.has_key('axis') else 0
    env = kwargs['env'] if kwargs.has_key('env') else "Name"
    prefix = kwargs['Image'] if kwargs.has_key('Image') else 'Image'
    displayrange = kwargs['displayrange'] if kwargs.has_key('displayrange') else [0, 0]
    indexrange = kwargs['indexrange'] if kwargs.has_key('indexrange') else [0, 15]
    nrow = kwargs['nrow'] if kwargs.has_key('nrow') else 5


    for i, tensor in enumerate(args):
        temp = tensor.transpose(*np.roll(range(3), -axis).tolist())
        if displayrange == [0, 0]:
            drange = [0, 0]
            drange[0] = temp.min()
            drange[1] = temp.max() + 0.1
        else:
            drange = displayrange
        temp = Normalize(temp, drange[0], drange[1])
        newRange = [max(0, indexrange[0]), min(indexrange[1], temp.shape[0])]
        newRange = [min(newRange[0], temp.shape[0] - 1), min(newRange[1], temp.shape[0])]

        vis.images(temp[:, None][newRange[0]:newRange[1]],
                   nrow=nrow, env=env, win=prefix+"%i"%i, padding=1)


if __name__ == '__main__':
    rootdir = "/media/storage/Source/Repos/CT-Rebinning-Toolkit/ProjectionData/00.RAW/L333/full_DICOM-CT-PD/"
    r=Reader()
    p=r.Read(rootdir, verbose=True, dtype=np.int16)

# if __name__ == '__main__':
#     rootdir = "/media/storage/Source/Repos/CT-Rebinning-Toolkit/ProjectionData/00.RAW/L067/full_DICOM-CT-PD/"
#     r = Reader()
#     p = r.Read(rootdir, verbose=True, dtype=np.int16)
#     p.saveMetadata("/media/storage/Source/Repos/CT-Rebinning-Toolkit/ProjectionData/10.Batch_01/testing/full.csv")



# if __name__ == '__main__':
#     rootdir = "/media/storage/Source/Repos/CT-Rebinning-Toolkit/ProjectionData/10.Batch_01/testing/"
#     subdirs = ['full_DICOM-CT-PD', 'quarter_DICOM-CT-PD', 'quarter_DICOM-CT-PD_Processed', 'quarter_DICOM-CT-PD', 'quarter_DICOM-CT-PD_Processed']
#
#     proj = []
#     for d in subdirs:
#         r = Reader()
#         p = r.Read(rootdir + "/" + d, verbose=True, dtype=np.float32, readRange=[26048, 28158, 100])
#         proj.append(p._data)
#         # print np.rad2deg(np.array(p._metadata['DetectorFocalCenterAngularPosition']))
#     proj = np.array(zip(proj)).squeeze()
#     proj = proj.transpose(1, 0, 2, 3)
#     proj[:,3] -= proj[:, 0]
#     proj[:,4] -= proj[:, 0]
#     proj[:, 1:3] = Normalize(proj[:,1:3])
#     proj[:,3:5]  = Normalize(abs(proj[:,3:5]), 0, abs(proj[:,3:5]).max() * 0.5)
#
#     Visualize2D(*proj[:,1::], nrow=4, env='Figures')
