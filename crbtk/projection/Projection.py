from pydicom import dcmread
import pydicom
import pandas as pd
from numpy import ndarray, all, array, save, load
from SimpleITK import GetImageFromArray, GetArrayFromImage, WriteImage, ReadImage


class Projection(object):
    def __init__(self):
        """Projection is a 2D data array wraped with metadata"""
        self.dectgeom = {}
        self.srcgeom = {}
        self.D = None           # Source-dector distance
        self.R = None           # Source-isocenter distance
        self._metadata = None
        self._data = None       # numproj, column, channel
        pass

    def __getitem__(self, item):
        """
        Data is arranged in order (Projection, columns, channels)

        :param item:
        :return:
        """
        assert not self._data is None, "Not initialized!"

        if isinstance(item, tuple):
            pass
        else:
            return self._data[item]

    def saveMetadata(self, fname):
        """
        Description
        -----------
          Save the meta data to csv

        :param fname:
        :return:
        """

        assert not self._metadata is None, "Not initialized!"
        assert isinstance(self._metadata, pd.DataFrame), "Incorrect data format!"

        self._metadata.to_csv(fname, index=False)
        save(fname.replace('.csv', '.npy'),{'srcgeom': self.srcgeom, 'dectgeom': self.dectgeom})

    def readMetadata(self, fname):
        self._metadata = pd.read_csv(fname)

    def readGeom(self, fname):
        geoms=load(fname)
        self.srcgeom=geoms.item().get('srcgeom')
        self.dectgeom=geoms.item().get('dectgeom')

    def getNumberOfProjections(self):

        assert isinstance(self._data, ndarray), "Projection is empty or corrupted"

        return self._data.shape[0]

    def saveAsNii(self, descfname, savemetadata=False):
        """

        :param descfname:
        :param savemetadata:
        :return:
        """

        assert self.srcgeom['mode'] == 'STACK', 'Currently only support non-helical scans'

        im = GetImageFromArray(self._data)
        im.SetSpacing([self.dectgeom['ColumnSpacing'],
                       1,
                       abs(self.srcgeom['z_0'][1]-self.srcgeom['z_0'][0])
                       ])
        if descfname.endswith('.nii'):
            descfname.replace('.nii', '.nii.gz')

        if not descfname.endswith('.gz'):
            descfname += '.nii.gz'

        WriteImage(im, descfname)
        if savemetadata:
            self.saveMetadata(descfname.replace('.nii.gz', '.csv'))