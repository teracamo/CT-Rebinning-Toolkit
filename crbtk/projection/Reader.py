import os
import fnmatch
import numpy as np
import pandas as pd


from tqdm import tqdm
from pydicom import dcmread
from Projection import Projection

from ..dect_geom import *


class Reader(object):
    def __init__(self):
        super(Reader, self).__init__()
        self._extension = ".dcm"
        pass

    def Read(self, dir, verbose=False, dtype=np.float16, readRange=None):
        """Read(dir)"""
        assert os.path.isdir(dir), "Cannot open directory!"

        out = Projection()

        files = os.listdir(dir)
        files = fnmatch.filter(files, "*" + self._extension)
        files.sort()

        if not readRange is None:
            assert readRange[0] < readRange[1]
            np.clip(readRange, 0, len(files))
            if len(readRange) == 3:
                files = files[readRange[0]:readRange[1]:readRange[2]]
            else:
                files = files[readRange[0]:readRange[1]]

        data = {'arr': [], 'metadata': pd.DataFrame(columns=[
            'InstanceNumber', 'RescaleIntercept', 'RescaleSlope', 'DetectorFocalCenterAngularPosition',
            'DetectorFocalCenterAxialPosition', 'DetectorFocalCenterRadialDistance', 'SourceAngularPositionShift',
            'SourceAxialPositionShift', 'SourceRadialDistanceShift'
        ])}
        k = dcmread(dir + "/" + files[0])
        data['arr']=np.zeros([len(files), k.NumberofDetectorColumns, k.NumberofDetectorRows], dtype=dtype)
        C = k.RescaleIntercept
        M = k.RescaleSlope
        for i, f in enumerate(tqdm([dir + "/" + f for f in files], disable=not verbose, desc='Reading...')):
            try:
                l_k = dcmread(f, force=True)
            except:
                tqdm.write("Error! Skipping %s"%f)
            data['metadata'].loc[i] = [l_k.InstanceNumber, C, M, l_k.DetectorFocalCenterAngularPosition,
                                   l_k.DetectorFocalCenterAxialPosition, l_k.DetectorFocalCenterRadialDistance,
                                   l_k.SourceAngularPositionShift, l_k.SourceAxialPositionShift, l_k.SourceRadialDistanceShift]
            data['arr'][i]=np.copy(l_k.pixel_array[None,:].astype(dtype))
            # del k.PixelData # delete attribute to save memory/

        # Add some additional information to projection
        try:
            tablefeed = data['metadata'].loc[k.NumberofSourceAngularSteps - 1]['DetectorFocalCenterAxialPosition'] - \
                        data['metadata'].loc[0]['DetectorFocalCenterAxialPosition']
        except KeyError:
            tablefeed = abs(data['metadata'].loc[1]['DetectorFocalCenterAxialPosition'] - \
                        data['metadata'].loc[0]['DetectorFocalCenterAxialPosition']) * (k.NumberofSourceAngularSteps - 1)

        out.D = k.DetectorFocalCenterRadialDistance
        out.R = out.D / 2.
        out.srcgeom = {'mode': k.TypeofProjectionData,
                       'geom': k.TypeofProjectionGeometry,
                       'numOfProj': k.NumberofSourceAngularSteps,
                       'pitch':  tablefeed ,
                       'sdd': k.ConstantRadialDistance,
                       'sod': k.DetectorFocalCenterRadialDistance,
                       'z_0': k.DetectorFocalCenterAxialPosition,
                       'calfactor': k.HUCalibrationFactor}
        out.dectgeom = {'Channel': k.NumberofDetectorRows,
                        'Columns': k.NumberofDetectorColumns,
                        'ChannelSpacing': k.DetectorElementAxialSpacing,
                        'ColumnSpacing': k.DetectorElementTransverseSpacing,
                        'ChannelRange': [- k.DetectorElementAxialSpacing * k.NumberofDetectorRows/ 2.,
                                           k.DetectorElementAxialSpacing * k.NumberofDetectorRows/ 2.],
                        'ColumnRange': [- k.DetectorElementTransverseSpacing * k.NumberofDetectorColumns/ 2.,
                                           k.DetectorElementTransverseSpacing * k.NumberofDetectorColumns/ 2.],
                        'FOV': k.DataCollectionDiameter,
                        'DectShape': k.DetectorShape,
                        'DectCentralElement': k.DetectorCentralElement
                        }

        data['arr'] = data['arr'] * M + C
        out._data = data['arr']
        out._metadata = data['metadata']
        return out

