import numpy as np
import pandas as pd
from scipy.interpolate import interp2d
from ...projection import Projection

def dectCyl2par(proj):
    assert isinstance(proj, Projection), "Input must be Projection object"
    assert proj.dectgeom['DectShape'] == 'CYLINDRICAL'
    assert proj.srcgeom['mode'] == 'STACK'

    DSD=proj.srcgeom['sdd']
    alpha=proj.dectgeom['ColumnSpacing']*proj.dectgeom['Columns'] / (2*float(DSD))
    DSD_prim=alpha * DSD / np.tan(alpha)

    N_proj=proj.srcgeom['numOfProj']
    N_col=proj.dectgeom['Columns']
    s_col=proj.dectgeom['ColumnSpacing']
    u_prim=np.arange(N_col) * s_col - N_col * s_col / 2.
    u=DSD * np.arctan(u_prim/DSD_prim)
    d=np.sqrt(u_prim**2+DSD_prim**2)/DSD

    out=Projection()
    out._data=np.zeros(proj._data.shape)
    out._metadata=pd.DataFrame.copy(proj._metadata)
    out.srcgeom=dict(proj.srcgeom)
    out.dectgeom=dict(proj.dectgeom)
    out.dectgeom['DectShape'] = 'FLAT'
    out.dectgeom['sdd'] = DSD_prim

    for i in xrange(out._data.shape[0]):
        x, y = np.arange(N_col) * s_col - N_col * s_col / 2., np.arange(N_proj)
        sino=interp2d(x, y,proj._data[i], fill_value=0)
        dd=sino(u, y)
        out._data[i]=np.concatenate([d[None,:] for j in xrange(len(y))], 0)*dd
    return out
