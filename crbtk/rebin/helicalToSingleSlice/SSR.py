import numpy as np
from tqdm import tqdm
from crbtk.projection import Projection
from scipy.interpolate import interp2d, RectBivariateSpline
from SimpleITK import WriteImage, GetImageFromArray
import pandas as pd

import matplotlib.pyplot as plt
plt.ion()

def GetNeighborIndexes(center, radius, boundingshape):
    """
    Get neighbourhood index with bounds checking in forms of [[min,max],[min,max]...]
    """
    assert radius > 0
    assert np.all(boundingshape > 0)
    return np.array([[max(0, center[i]-radius), min(boundingshape[i], center[i]+radius+1)] for i in xrange(len(boundingshape))])

def SSR(projection, num_of_slice, attenuation_correction=False, filename=None, downsample=-1):
    # assert isinstance(projection, Projection)

    # define range of z = z_0 +- P/2
    P = projection.srcgeom['pitch']
    z_coords = np.array(projection._metadata['DetectorFocalCenterAxialPosition'])   # Range of whole helical path
    z_offsets = np.array(projection._metadata['SourceAxialPositionShift'])          # Offset from projection raw data
    dsd_offsets = np.array(projection._metadata['SourceRadialDistanceShift'])       # Offset from projection raw data
    centralelement_offset = np.array(projection.dectgeom['DectCentralElement'])-np.array([projection.dectgeom['Columns'], projection.dectgeom['Channel']])/2.
    centralelement_offset = centralelement_offset * np.array([projection.dectgeom['ColumnSpacing'], projection.dectgeom['ChannelSpacing']])

    z_range = [z_coords[0] + P / 2., z_coords[-1] - P / 2.] # Range of slice reconstruction
    z_slices_coords = np.linspace(z_range[0], z_range[1], num_of_slice)


    angles = np.array(projection._metadata['DetectorFocalCenterAngularPosition']) + \
             np.array(projection._metadata['SourceAngularPositionShift'])
    sod = np.array(projection._metadata['DetectorFocalCenterAxialPosition'])
    detector_coords = np.array(zip(angles, z_coords, sod, z_offsets, dsd_offsets))
    src_coords = np.array(detector_coords)
    src_coords[:,0] += np.pi
    src_coords[:,2] = projection.srcgeom['sod']
    # print src_coords.shape

    # clean projections for infinity pixels
    infvals=np.argwhere(np.isinf(projection._data))
    projection._data[infvals]=0
    if len(infvals) > 0:
        print "Warning! Find inf values in projections, trying to smooth!"
        for index in infvals:
            indexes=GetNeighborIndexes(index[1::], 1, projection._data.shape[1::])
            x, y=np.meshgrid(np.arange(*indexes[0]), np.arange(*indexes[1]))
            indexes=zip(x.flatten(), y.flatten())
            projection._data[index[0], index[1], index[2]]=np.sum([projection._data[index[0],x, y] if [x, y] != index[1::].tolist() \
                                                                       else 0 for x, y in indexes]) / float(len(indexes)-1) # x and y is inverted for unknown reasons
        print "Successfully smoothed: ", infvals.tolist()
    nanvals=np.argwhere(np.isnan(projection._data))
    projection._data[nanvals]=0
    if len(infvals) > 0:
        print "Warning! Find nan values in projections, trying to smooth!"
        for index in nanvals:
            indexes=GetNeighborIndexes(index[1::], 1, projection._data.shape[1::])
            x, y=np.meshgrid(np.arange(*indexes[0]), np.arange(*indexes[1]))
            indexes=zip(x.flatten(), y.flatten())
            projection._data[index[0], index[1], index[2]]=np.sum([projection._data[index[0],x, y] if [x, y] != index[1::].tolist() \
                                                                       else 0 for x, y in indexes]) / float(len(indexes)-1) # x and y is inverted for unknown reasons
        print "Successfully smoothed: ", infvals.tolist()

    metadata=pd.DataFrame(columns=[
        'InstanceNumber',  'RescaleIntercept', 'RescaleSlope', 'DetectorFocalCenterAngularPosition',
        'DetectorFocalCenterAxialPosition', 'DetectorFocalCenterRadialDistance', 'SourceAngularPositionShift',
        'SourceAxialPositionShift', 'SourceRadialDistanceShift'
    ])
    fan_src_angles = []
    outnii = []
    # for each z slice, find the set of cone beam source that is close to each angular position of the fan-beam source
    for i, Z_prime in enumerate(tqdm(z_slices_coords, desc="slice")):
        # get projections correspond with Z
        factor_z = (Z_prime - z_coords[0]) / abs(z_coords[-1] - z_coords[0])
        proj_index_center = int(projection.getNumberOfProjections() * factor_z)
        proj_index_range = [proj_index_center - int(np.floor(projection.srcgeom['numOfProj'] / 2.)),
                            proj_index_center + int(np.ceil(projection.srcgeom['numOfProj'] / 2.)) - 1]

        slice_metadata=pd.DataFrame.copy(projection._metadata.loc[proj_index_range[0]:proj_index_range[1]])
        slice_metadata['SliceNumber']=i
        cone_src_coords = src_coords[proj_index_range[0]:proj_index_range[1]+1]
        cone_projections = projection._data[proj_index_range[0]:proj_index_range[1]+1]

        fan_src_coords = cone_src_coords.copy()
        fan_src_coords[:,2] = Z_prime
        fan_proj = []
        for j, cp in enumerate(zip(cone_src_coords, cone_projections)):
            delta_z = cp[0][1] + cp[0][3] -  Z_prime # del_z = z_0 + z_offset - Z_prime

            u = np.linspace(projection.dectgeom['ColumnRange'][0],
                            projection.dectgeom['ColumnRange'][1],
                            projection.dectgeom['Columns'])
            D = float(projection.srcgeom['sdd']) + cp[0][4]
            R = float(projection.srcgeom['sod']) + cp[0][4]
            if projection.dectgeom['DectShape'] == 'CYLINDRICAL':
                v = delta_z * D / R
                W = D / np.sqrt(v**2 + D**2)
                cone_proj_interp = interp2d(np.linspace(projection.dectgeom['ChannelRange'][0],
                                                        projection.dectgeom['ChannelRange'][1] ,
                                                        projection.dectgeom['Channel']) - centralelement_offset[1],
                                            np.linspace(projection.dectgeom['ColumnRange'][0],
                                                        projection.dectgeom['ColumnRange'][1],
                                                        projection.dectgeom['Columns']) - centralelement_offset[0],
                                            cp[1].astype('float32'), fill_value=0)
                g = W * cone_proj_interp(v, u)[None,:]
                if np.isnan(g).any():
                    tqdm.write("Series error! z:%s j:%s has nan"%(i,j))
                if np.isinf(g).any():
                    tqdm.write("Series error! z:%s j:%s has inf"%(i,j))
            else:
                raise NotImplementedError
            fan_proj.append(g)
        fan_proj = np.concatenate(fan_proj, axis=0).squeeze()

        # Roll the fan_proj along the phi axis to align the angle represented by first row
        # off_rows = proj_index_range[0] % projection.srcgeom['numOfProj'] - int((np.abs(angles[0]) % (2*np.pi))/(2*np.pi / float(projection.srcgeom['numOfProj'])))
        off_rows = np.int(np.ceil(-angles[proj_index_range[0]] * projection.srcgeom['numOfProj'] / (2* np.pi)))
        fan_proj = np.roll(fan_proj, off_rows, 0)[None,:].astype('float32')
        slice_metadata=slice_metadata.reindex(np.roll(slice_metadata.index, off_rows))
        fan_src_angles.append(fan_src_coords[:,0][None,:])

        # flip to align the sign of axis and sign of angles
        fan_proj = np.flip(fan_proj, axis=1)
        # fan_proj = np.flip(fan_proj, axis=2)
        if downsample > 0:
            indexes=np.linspace(0, projection.srcgeom['numOfProj']-1, downsample)
            indexes=np.array(np.round(indexes), dtype=int)
            bs=np.zeros(fan_proj.shape[1], dtype=np.bool)
            for x in indexes:
                bs[x]=True
            fan_proj=fan_proj[:,bs]
            slice_metadata=slice_metadata.loc[bs]
            pass

        outnii.append(fan_proj)
        metadata=metadata.append(slice_metadata)



    outnii=np.concatenate(outnii, axis=0)
    if attenuation_correction:
        outnii = outnii * projection.srcgeom['sdd'] / 10.
    if not filename is None:
        # save the output as nii
        outnii = GetImageFromArray(outnii)
        outnii.SetSpacing([1,1, abs(z_slices_coords[2]-z_slices_coords[1])])
        WriteImage(outnii, filename)

        # save the metadata of the fan-beam
        savedict = dict(projection.dectgeom)
        savedict['sdd'] = projection.srcgeom['sdd']
        savedict['sod'] = projection.srcgeom['sod']
        savedict['z_0'] = projection.srcgeom['z_0']
        savedict['phi'] = np.concatenate(fan_src_angles, axis=0)
        savedict['calfactor'] = projection.srcgeom['calfactor']
        np.save(filename.replace('.nii.gz', '.dict'), savedict)

    out = Projection()
    out._data = outnii
    out.srcgeom = dict(projection.srcgeom)
    out.dectgeom = dict(projection.dectgeom)
    out.srcgeom['mode'] = 'STACK'
    out.srcgeom['geom'] = 'FANBEAM'
    out.srcgeom['pitch'] = 0
    out.srcgeom['z_0'] = z_slices_coords
    if downsample > 0:
        out.srcgeom['numOfProj']=downsample
    out._metadata=metadata
    return out