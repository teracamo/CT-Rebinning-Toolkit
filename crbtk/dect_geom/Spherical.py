import numpy as np
from numpy import arcsin, arccos, sin, cos, pi
from DectGeom import DectGeom

class Spherical(DectGeom):
    """Flat panel detectors"""
    def __init__(self, focus_dect_dist, dectpos, w, h, dimW, dimH, tilt):
        super(Spherical, self).__init__(focus_dect_dist, dectpos, w, h, dimW, dimH, tilt)

        self._fanAngle_w = w / (self._focus_dect_dist * 2 * pi)
        self._fanAngle_h = h / (self._focus_dect_dist * 2 * pi)

        pass

    def toCartesian(self, input):
        """Return cartesian representation of the input coordinate"""
        assert len(input) == 2
        assert input[0] >= 0 & input[0] < self._dimW
        assert input[1] >= 0 & input[1] < self._dimH
        input -= self.getDim() /2.
        angle = input[0] * self._pixelSize[0] / (float(self._focus_dect_dist) * 4 * pi)
        offset = np.array([sin(angle), cos(angle)])
        offset = -self._focus_dect_dist * offset + np.array([0, self._focus_dect_dist])
        input = np.array([offset[0], offset[1], input[1] * self._pixelSize[1], 1])
        return self.get2CartesianTransform().dot(input)

    def fromCartesian(self, input):
        """Inverse of toCartesian"""
        assert len(input) == 4
        e = self.getFromCartesianTransform().dot(input)
        return np.array(e[::2] / self._pixelSize + self.getDim() / 2., dtype=int)

    def buildTransformMatrix(self):
        """Get the matrix from dector pixel to cartesian coordinate"""
        alpha = self.getProjectionAngle()
        offset = self._dectpos
        t = np.array([[cos(alpha), -sin(alpha), 0, offset[0]],
                      [sin(alpha), cos(alpha), 0, offset[1]],
                      [0, 0, 1, offset[2]],
                      [0, 0, 0, 1]])
        self._matrix = t

DectGeom.register(Spherical)
