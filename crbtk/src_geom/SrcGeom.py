import abc
import numpy as np

class SrcGeom(object):
    __metaclass__ = abc.ABCMeta
    def __init__(self, isocenter, srcpos, dectorGeom):
        """SrcGeom(isocenter, srcpos, dectorGeom)

        :param np.ndarray isocenter:
        :param np.ndarray srcpos:
        :param DectGeom dectorGeom:
        """

    @abc.abstractmethod
    def toCartesian(self, input):
        """Return cartesian representation of the input coordinate"""
        return

    @abc.abstractmethod
    def fromCartesian(self, input):
        """Return the geometric representation of input coordinate using the convention of this class"""
        return

