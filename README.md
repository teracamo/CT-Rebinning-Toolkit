# CT-Rebinning-Toolkit

This toolkit is developed to load and rebin DICOM series formatted in standard of [1]. For more information, refer to 
the publication in [url]https://www.ncbi.nlm.nih.gov/pubmed/27239087 

# Usage

## Requirements

The following python libraries are required:
- pydicom
- numpy
- scipy
- SimpleITK

## I/O

### Load DICOM Projection Data

```py
from crbtk.projection import reader
import os

if __name__ == '__main__':
    r = reader("DICOM/DIRECTORY")
    r.setSeries('SeriesSOPClassID')
    
    pro = reader.read() # a crbtk.Projection object
    
```

### Save Rebinned Projection Data



# References
[1] Chen, Baiyu, et al. "An open library of CT patient projection data." _Medical Imaging 2016: Physics of Medical 
Imaging._ Vol. 9783. International Society for Optics and Photonics, 2016.